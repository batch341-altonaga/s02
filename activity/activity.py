# S02 Activity:
# 1. Get a year from the user and determine if it is a leap year or not.

# Input error checking
while True:
    try:
        year = int(input("Please enter a year: "))
        if year <= 0:
            print("Please enter a positive integer")
        else:
            break
    except ValueError:
        print("Please enter an integer")

# Leap year checking
if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print(f"{year} is a leap year")
        else:
            print(f"{year} is not a leap year")
    else:
        print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")

# 2.  Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.
col = int(input("Please enter the number of columns: "))
row = int(input("Please enter the number of rows: "))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()

